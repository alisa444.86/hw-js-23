const table = document.createElement('table');

for (let i = 0; i < 30; i++) {
    const row = document.createElement('tr');
    row.classList.add('table-row');

    for (let x = 0; x < 30; x++) {
        const cell = document.createElement('td');
        cell.classList.add('table-cell');
        row.appendChild(cell);
    }

    table.prepend(row);
}

document.body.prepend(table);

table.addEventListener('click', (event) => {
    if (event.target.classList.contains('table-cell')) {
        event.target.classList.toggle('black');
    }
});

document.body.addEventListener('click', (event) => {
    if (event.currentTarget !== table && !event.target.classList.contains('table-cell')) {
        table.classList.toggle('reverse');
    }
});
